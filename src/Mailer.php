<?php

namespace lucatomasi\Mailer;

use PHPMailer;
use phpmailerException;

/**
 * @author Luca Tomasi | lucatomasi77@gmail.com
 * @version 1.0.0
*/

/**
 * Class Mailer
 * @package lucatomasi\Mailer
 */
class Mailer {

    /**
     * @var PHPMailer $mailer
     */
    private static $mailer = null;

    /**
     * @var bool
     */
    private static $debug;

    /**
     * Mailer constructor.
     * @param string $host
     * @param string $username
     * @param string $password
     * @param boolean $debug
     * @param int $port
     */
    public function __construct(string $host, string $username, string $password, $debug=false, $port=587) {

        self::$debug = $debug;

        self::$mailer = new PHPMailer($debug);                  // Passing `true` enables exceptions

        // Server settings
        self::$mailer->CharSet = "UTF-8";
        self::$mailer->Port = $port;                            // TCP port to connect to

        self::$mailer->isSMTP();                                // Set mailer to use SMTP
        self::$mailer->SMTPAuth = true;                         // Enable SMTP authentication
        self::$mailer->Host = $host;                            // Specify main and backup SMTP servers
        self::$mailer->Username = $username;                    // SMTP username
        self::$mailer->Password = $password;                    // SMTP password
        self::$mailer->SMTPSecure = 'tls';                      // Enable TLS encryption, `ssl` also accepted
        self::$mailer->SMTPOptions = array(
            'ssl' => [
                'verify_peer' => false,
                'allow_self_signed' => true,
                'peer_name' => $host
            ]
        );

    }

    /**
     * @param string $receiverEmail
     * @param string $subject
     * @param string $content
     * @param string $replyToEmail
     * @param string|null $replyToName
     * @param array|null $filePaths
     * @return bool
     * @throws phpmailerException
     */
    public static function sendMail(string $receiverEmail, string $subject, string $content, string $replyToEmail, string $replyToName=null, array $filePaths=null): bool {
        try {
            self::$mailer->clearAttachments();

            // Recipients
            self::$mailer->setFrom($replyToEmail);
            self::$mailer->addAddress($receiverEmail);                      // Add a recipient
            if (!is_null($replyToEmail)) {
                if (is_null($replyToName)) $replyToName = $replyToEmail;
                self::$mailer->addReplyTo($replyToEmail, $replyToName);
            }

            // Attachments
            if (!is_null($filePaths)) {
                foreach ($filePaths as $file) {
                    self::$mailer->addAttachment($file, basename($file));
                }
            }

            // Content
            self::$mailer->isHTML(true);                              // Set email format to HTML
            self::$mailer->Subject = $subject;
            self::$mailer->Body = $content;
            self::$mailer->send();
            return true;
        } catch (phpmailerException $e) {
            if (self::$debug) throw $e;
            return false;
        }
    }

}
